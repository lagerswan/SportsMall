<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>购物车</title>
		<style>
			
		</style>
		<link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_1493843_jx3cr7mqjfp.css"/>
	</head>
	<body>
		<table border="" cellspacing="0" cellpadding="0" style="margin: 0 auto;text-align: center;">
			<tr>
				<td colspan="5"style="background-color: bisque;">购物车列表</td>
				<c:if test="${empty(cart.map) }">您还没购任何商品</c:if>
			</tr>	
   <c:if test="${!empty(cart.map) }"> 
  
			<tr style="background-color: yellow;">
				<td>商品名称</td>
				<td>单价/元</td>
				<td>数量</td>
				<td>小计</td>
				<td>操作</td>
			</tr>
			<c:forEach items="${cart.map}" var="entry">
			<tr>
				<td>${entry.value.product.name}</td>
				<td>${entry.value.product.discountprice}</td>
				<td>${entry.value.count}</td>
				<td>${entry.value.price}</td>
				<td><a href="deleteItem?id=${entry.value.product.id}">删除</a></td>
			</tr>
			</c:forEach>
			<tr>
				<td colspan="2">总价</td>
				<td colspan="2">${cart.price}</td>
				<td>清空购物车</td>
			</tr>
			
			</c:if>
			<tr>
				<td colspan="5"><a href=""><button id="btn">结算</button></a></td>
			</tr>
		</table>

	</body>
</html>