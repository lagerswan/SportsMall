package SportsMall.dao.product;

import java.util.List;

import SportsMall.entity.Product;


public interface ProductDao {
	public List<Product> findAll_zuqiu();
	public List<Product> finAll_lanqiu();
	public List<Product> finAll_yumaoqiu();
	public List<Product> finAll_pingpangqiu();
	public Product selectProduct_zq(int id);

}
